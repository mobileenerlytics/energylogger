package com.mobileenerlytics.eprof.logger;

public interface IEprofChangeListener {
	public void startedEprof();
	public void stoppedEprof(boolean success, String logDir);
	public void resettedEprof(String logDir);
	public void processedTraces(boolean success);
}
