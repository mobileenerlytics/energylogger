package com.mobileenerlytics.eprof.logger;

import com.mobileenerlytics.eprof.logger.EprofLoggerMain.LoggedComponent;
import com.mobileenerlytics.eprof.logger.bridge.ClientBridge;
import com.mobileenerlytics.eprof.logger.bridge.IDeviceBridge;
import com.mobileenerlytics.eprof.logger.huawei.HwScreenRecorder;
import com.mobileenerlytics.eprof.logger.huawei.MaliGPUPoller;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class EprofLogger {
	protected EprofLoggerMain mLoggerMain;
	protected IDeviceBridge mDevice;
	protected Set<LoggedComponent> forComponent;

	public EprofLogger( EprofLoggerMain loggerMain ){
		mLoggerMain = loggerMain;
		mDevice = mLoggerMain.mDevice;
	}
	
	public void prepareDevice() throws Exception {
		
	}
	
	// Start logging
	public abstract boolean start();

	// Stop logging
	public abstract void stop();
	
	// Reset Logger
	public abstract void reset();
	
	/* Add and remove shall only be called for clients that should be traced*/
	public void addClient(ClientBridge client) {
	}
	
	public void removeClient(ClientBridge client) {
	}

    public static Collection<? extends EprofLogger> getDeviceSpecificLogger(
    		EprofLoggerMain loggerMain, String property) {
		List<EprofLogger> ret = new LinkedList<>();
		if (property == null) property = "";
	    switch (property) {
			case "HWWAS-H":
				ret.add(new MaliGPUPoller(loggerMain));
				ret.add(new HwScreenRecorder(loggerMain));
				break;
			default:
				ret.add(new ScreenRecorder(loggerMain));
				break;
		}
		return ret;
    }
}