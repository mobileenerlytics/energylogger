package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

//import com.mobileenerlytics.eprof.logger.bridge.IATraceReader;

public class Atrace extends EprofLogger {
    private static final String TRACEREDUCE = "tracereduce";
    private static final String ATRACE_INT = "pkill " + TRACEREDUCE;

    private static final String ATRACE_START = "atrace --async_start freq idle sync";
    private static final String ATRACE_STOP = "atrace -t 1";
    public static final String tracingDir = "/sys/kernel/debug/tracing/";
    private static final String k_bufferSize = tracingDir + "buffer_size_kb";
    private static final String k_tracePipePath = tracingDir + "trace_pipe";
    private static final String k_setEventPath = tracingDir + "set_event";

    private static final String BUFFER_SZ_CMD = "echo 16384 > " + k_bufferSize;
    private static final String SCHED_CMD = "echo sched:sched_switch >> " + k_setEventPath;
    private static final String KGSL_CMD = "echo kgsl:kgsl_pwrlevel kgsl:kgsl_pwr_set_state kgsl:kgsl_buslevel >> " +
            k_setEventPath;
    private static final String SDCARD_CMD = "echo " +
            "writeback:writeback_start writeback:writeback_written " +
            "writeback:writeback_queue_io writeback:writeback_write_inode " +
            "writeback:writeback_write_inode_start writeback:writeback_single_inode " +
            "writeback:writeback_single_inode_start " +
            "writeback:writeback_wake_background >> " +
            k_setEventPath;

    private static final String[] HW_DECODER_CMDS = new String[]{"echo 0x0005 > /d/msm_vidc/fw_level",
            "echo 0x1000 > /d/msm_vidc/debug_level", "echo 1 > /d/msm_vidc/debug_output" };
    private static final String TAG = "Atrace";
    private static final Logger logger = LoggerFactory.getLogger(TAG);

    private String remoteAtrace;
//    volatile IATraceReader traceReader;
    volatile Future<String> traceFuture;

    public Atrace(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.CPU, EprofLoggerMain.LoggedComponent.GPU);
    }

    @Override
    public void prepareDevice() throws Exception {
        URL url = getClass().getClassLoader().getResource(TRACEREDUCE);
        mDevice.pushScript(url);
    }

    private void startAtrace() throws InterruptedException {
        try {
            mDevice.executeShellCommand(ATRACE_START, 10, TimeUnit.SECONDS).get();
            mDevice.executeShellCommand(SCHED_CMD, 10, TimeUnit.SECONDS).get();
            mDevice.executeShellCommand(KGSL_CMD, 10, TimeUnit.SECONDS).get();
            mDevice.executeShellCommand(SDCARD_CMD, 10, TimeUnit.SECONDS).get();
            for(String cmd : HW_DECODER_CMDS) {
                mDevice.executeShellCommand(cmd, 10, TimeUnit.SECONDS).get();
            }
            mDevice.executeShellCommand(BUFFER_SZ_CMD, 10, TimeUnit.SECONDS).get();
        } catch (Exception e) {
            logger.error("Interruption in startAtrace");
            throw new InterruptedException();
        }
    }

    @Override
    public boolean start() {
        remoteAtrace = mLoggerMain.getLogDir() + "/atrace";
        try {
            mDevice.executeShellCommand(ATRACE_STOP, 10, TimeUnit.SECONDS).get();
            traceFuture = mDevice.runScript(TRACEREDUCE, new String[]{k_tracePipePath, remoteAtrace});
//            traceReader = mDevice.getAtraceReader();
//            traceReader.asyncRead(k_tracePipePath, remoteAtrace);
            startAtrace();
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return true;
    }

    @Override
    public void stop() {
//        _stop(true);
        _stop();
        TraceFinishThread finishThread = new TraceFinishThread();
        finishThread.start();
    }

    @Override
    public void reset() {
        _stop();
    }

    private void _stop() {
        try {
//            logger.info("Stopping atrace");
            logger.info("Sending SIGTERM to TraceReduce");
            Future<String> stopAtracer = mDevice.executeShellCommand(ATRACE_INT, 10, TimeUnit.SECONDS);
            // Stop trace reader before stopping the atrace. Stopping atrace first might block
            // fread of trace_pipe in traceReader
//            traceReader.syncStop(graceful);
            stopAtracer.get();
            Future<String> stopPipe = mDevice.executeShellCommand(ATRACE_STOP, 10, TimeUnit.SECONDS);
            stopPipe.get();
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    class TraceFinishThread extends Thread {
        TraceFinishThread() {
            super("TraceFinish Thread " );
        }
        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                logger.error(e.toString());
            }
            logger.info("Killing TraceReduce");
            if (traceFuture != null) {
                traceFuture.cancel(true);
            }
            mLoggerMain.finish(Atrace.this);
        }
    }
}
