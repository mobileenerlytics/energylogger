package com.mobileenerlytics.eprof.logger.util;

import java.io.PrintWriter;

import com.mobileenerlytics.eprof.logger.EprofLoggerMain;

public class LoggerRunnable implements Runnable {
	String trace;
	PrintWriter pw;
	EprofLoggerMain mLoggerMain;
	public LoggerRunnable(EprofLoggerMain loggerMain, String trace, PrintWriter pw) {
		mLoggerMain = loggerMain;
		this.trace = trace;
		this.pw = pw;
	}
	@Override
	public void run() {
		String timestamp = mLoggerMain.getDeviceTime();
		trace = timestamp + " " + trace;
		pw.println(trace);
	}
}
