package com.mobileenerlytics.eprof.logger.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipInputStream;

public class Util {
	private static final String TAG = "Util";
	private static final Logger log = LoggerFactory.getLogger(TAG);

	/**
	 * Execute the command {@link pCmd} on a shell
	 * 
	 * @param pCmd
	 * @return 0 if all went fine, -1 otherwise
	 */
	public static int runCmd(String pCmd) {
		Runtime runtime;
		Process process;
		int retval = -1;

		try {
			runtime = Runtime.getRuntime();
			process = runtime.exec(new String[] { "sh", "-c", pCmd });
			retval = process.waitFor();
		} catch (Exception e) {
			log.error("Can't execute cmd \"{}\"", pCmd);
			log.error(e.toString());
		}

		return retval;
	}

	// public static boolean copyFileFromRAW(IDeviceBridge device, String pFilename) {
	// 	return copyFileFromRAW(device, pFilename, Config.PARENT_DIR, true);
	// }

	// public static boolean copyModuleFromRAW(IDeviceBridge device, String pFilename) {
	// 	return copyFileFromRAW(device, pFilename,
	// 			Config.Dirs.MODULES.getAbsolutePath(), false);
	// }

	// /**
	//  * Copy the file included in the apk-file (identified by {@link pRAWID}) to
	//  * the apps private data dir as file {@link pFilename}
	//  * 
	//  * @param pFilename
	//  *            the filename the raw resource should be copied to
	//  * @param pDir
	//  *            directory where it needs to be copied
	//  * @return true if all went fine, false otherwise
	//  */
	// private static boolean copyFileFromRAW(IDeviceBridge device, String pFilename, String pDir,
	// 		boolean setExecutable) {
	// 	log.debug("copying {} into {}", pFilename, pDir);
	// 	File file = new File("res" + File.separatorChar + "raw" + File.separatorChar + pFilename);
	// 	try {
	// 		device.pushFile(file.getAbsolutePath(), pDir + File.separatorChar + file.getName());
	// 	} catch (Exception e) {
	// 		log.error(e.toString());
	// 		return false;
	// 	}
	// 	return true;
	// }
	
	// class PidofShellReceiver implements IShellOutputReceiver {
	// 	private static final String TAG = "PidofShellReceiver";
	// 	public boolean done = false;
	// 	public LinkedList<Integer> list = new LinkedList<Integer>();
	// 	StringBuffer mOutputBuffer = new StringBuffer();
	// 	
	// 	@Override
	// 	public boolean isCancelled() {
	// 		return false;
	// 	}
	// 	@Override
	// 	public void flush() {
	// 		Log.d(TAG, "Parsing pidof output...");
	// 		String line = mOutputBuffer.toString();
	// 		StringTokenizer tokenizer = null;
	// 		Log.d(TAG, "-" + line + "-");
	// 		tokenizer = new StringTokenizer(line, " ");
	// 		while (tokenizer.hasMoreTokens()) {
	// 			try {
	// 				String token = tokenizer.nextToken();
	// 				token = token.replaceAll("\\s+", "");
	// 				list.add(Integer.valueOf(token));
	// 			} catch (NumberFormatException e) {
	// 				Log.e(TAG, e.getMessage());
	// 			}
	// 		}
	// 		done = true;
	// 	}
	// 	@Override
	// 	public void addOutput(byte[] data, int offset, int length) {
	// 		if (!isCancelled()) {
	//             String s = null;
	//             try {
	//                 s = new String(data, offset, length, "UTF-8"); //$NON-NLS-1$
	//             } catch (UnsupportedEncodingException e) {
	//                 // normal encoding didn't work, try the default one
	//                 s = new String(data, offset,length);
	//             }
	//             mOutputBuffer.append(s);
	//         }
	// 	}
	// }
	
	// /**
	//  * Get all pids of {@link pCmd}.
	//  * 
	//  * @param pContext
	//  * @param pCmd
	//  * @return if no process with name {@link pCmd} is running, an empty array
	//  *         will be returned. otherwise the array contains all process ids of
	//  *         this command. if an error occurs, null will be returned.
	//  */
	// public List<Integer> getProcessIDs(IDeviceBridge pDevice, String pCmd) {
	// 	try {
	// 		IDevice device = ((PluginDevice) pDevice).getDevice();
	// 		Log.d(TAG, "getProcessIDs for -" + pCmd + ",");
	// 		PidofShellReceiver receiver = new PidofShellReceiver();
	// 		device.executeShellCommand("pidof " + pCmd, receiver);
	// 		while( !receiver.done ){
	// 			Thread.sleep(500);
	// 		}
	// 		Log.d(TAG, "Got " + receiver.list.size() + " pids from pidof");
	// 		for(Integer pid : receiver.list){
	// 			Log.d(TAG, "pid: " + pid);
	// 		}
	// 		return receiver.list;
	// 	} catch (Exception e) {
	// 		Log.e(TAG, e.toString());
	// 	} 
	// 	return null;
	// }

	/*public static byte[] mkMD5Hash(String pFilename)
			throws NoSuchAlgorithmException, IOException {
		byte ret[];
		InputStream in = new FileInputStream(pFilename);

		ret = Util.mkMD5Hash(in);
		in.close();

		return ret;
	}

	public static byte[] mkMD5Hash(InputStream pIn)
			throws NoSuchAlgorithmException, IOException {
		MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
		int bytesRead = 0;
		byte buffer[] = new byte[512];
		do {
			bytesRead = pIn.read(buffer, 0, buffer.length);
			if (bytesRead > 0) {
				digest.update(buffer, 0, bytesRead);
			}
		} while (bytesRead > 0);

		return digest.digest();
	}*/
	
	public static void appendToFile(String srcFile, String appendToDestFile)
			throws IOException {
		OutputStream out = new FileOutputStream(appendToDestFile, true);
		byte[] buf = new byte[4096];
		InputStream in = new FileInputStream(srcFile);
		int b = 0;
		while ((b = in.read(buf)) >= 0) {
			out.write(buf, 0, b);
			out.flush();
		}
		in.close();
		out.close();
	}

	public static void extractFile(ZipInputStream zipIn, String filePath)
			throws IOException {
		int BUFFER_SIZE = 4096;
		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(filePath));
		byte[] bytesIn = new byte[BUFFER_SIZE];
		int read = 0;
		while ((read = zipIn.read(bytesIn)) != -1) {
			bos.write(bytesIn, 0, read);
		}
		bos.close();
	}

	public static File lastFileModified(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});
		long lastMod = Long.MIN_VALUE;
		File choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod) {
				choice = file;
				lastMod = file.lastModified();
			}
		}
		return choice;
	}
}
