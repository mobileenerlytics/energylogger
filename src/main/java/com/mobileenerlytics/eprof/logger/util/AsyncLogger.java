package com.mobileenerlytics.eprof.logger.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AsyncLogger {
	ThreadPoolExecutor executor;
	
	public AsyncLogger() {
		executor = new ThreadPoolExecutor(2, 4, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<Runnable>());
	}
	
	public void runAsync (Runnable runnable) {
		executor.execute(runnable);
	}
}
