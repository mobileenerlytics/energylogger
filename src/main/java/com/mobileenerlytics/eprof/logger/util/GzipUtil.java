package com.mobileenerlytics.eprof.logger.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class GzipUtil {
	public static void gunzipIt(String input_gzip_file, String output_file)
			throws IOException {
		byte[] buffer = new byte[1024];
		GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(
				input_gzip_file));
		FileOutputStream out = new FileOutputStream(output_file);

		int len;
		while ((len = gzis.read(buffer)) > 0) {
			out.write(buffer, 0, len);
		}

		gzis.close();
		out.close();
	}
}
