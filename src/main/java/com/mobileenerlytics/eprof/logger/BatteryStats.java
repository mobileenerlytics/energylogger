package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;

import java.util.HashSet;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class BatteryStats extends EprofLogger {
    private static final String RESET = "dumpsys batterystats --reset";
    private static final String START = "dumpsys batterystats --enable full-wake-history";  // For capturing wakelocks and alarms
    private static final String STOP = "dumpsys batterystats | tee %s/batterystats";

    private static final String DISABLE_CHARGING = "dumpsys battery unplug";
    private static final String RESET_CHARGING = "dumpsys battery reset";

    public BatteryStats(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.GPS, EprofLoggerMain.LoggedComponent.POWER_EVENT);
    }

    @Override
    public boolean start() {
        try {
            // If we reset, batterystats aren't even collected for short tests
            // But if we don't do reset, the trace may overflow without anyway to recover.
            // Also, the backend parser takes forever parsing things irrelevant to this test.
            Future<String> future = mDevice.executeShellCommand(RESET, 10, TimeUnit.SECONDS);
            future.get();
//            Future<String> future;
            future = mDevice.executeShellCommand(START, 10, TimeUnit.SECONDS);
            future.get();
            future = mDevice.executeShellCommand(DISABLE_CHARGING, 10, TimeUnit.SECONDS);
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void stop() {
        try {
            String stop = String.format(STOP, mLoggerMain.getLogDir());
            Future<String> future = mDevice.executeShellCommand(stop, 3, TimeUnit.MINUTES);
            future.get();
            future = mDevice.executeShellCommand(RESET_CHARGING, 10, TimeUnit.SECONDS);
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        // Do nothing
    }
}
