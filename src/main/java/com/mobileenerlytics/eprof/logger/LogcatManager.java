package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class LogcatManager extends EprofLogger {
	//public static final String STOP_LOGCAT = "killall logcat";
	public static final String LOGCAT_OUTPUT_NAME = "logcat_out";
	private final String CLEAR_LOGCAT = "logcat -c";
	private static final Logger LOG = LoggerFactory.getLogger("LogcatManager");
	private static Future<String> future;

	LogcatManager(EprofLoggerMain loggerMain) {
		super(loggerMain);
		forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.LOGCAT);
	}

	public boolean start() {
		try {
			final String LOGCAT_FILE = mLoggerMain.getLogDir() + LOGCAT_OUTPUT_NAME;
			final String START_LOGCAT = "logcat -v time \"*.v\" | tee " + LOGCAT_FILE + " 2>&1";
			future = mLoggerMain.mDevice.executeShellCommand(CLEAR_LOGCAT, 10, TimeUnit.SECONDS);
			future.get();
			future = mLoggerMain.mDevice.executeShellCommand(START_LOGCAT, 10, TimeUnit.HOURS);
			//future.get();
		} catch (Exception e) {
			LOG.error(e.toString());
			return false;
		}
		return true;
	}

	public void stop() {
//		String localLogcat = mLoggerMain.getLogDir() + LOGCAT_OUTPUT_NAME;
		try {
		    stop_();
//			mDevice.pullFile(LOGCAT_FILE, localLogcat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mLoggerMain.finish(this);
	}

	private void stop_() {
		try {
			if(future != null) {
				future.cancel(true);
				future = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			mDevice.executeShellCommand("pkill logcat", 10, TimeUnit.SECONDS).get();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void reset() {
	    stop_();
	}
}
