package com.mobileenerlytics.eprof.logger;

import com.google.common.base.Splitter;

import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ScreenRecorder extends EprofLogger {
    private static final String TAG = "ScreenRecorder";
    private static final Logger LOG = LoggerFactory.getLogger(TAG);
    private static final String SCREEN_CMD = "screenrecord --bugreport --bit-rate 1M --size 720x1280 ";
    private final static Splitter blankSplitter = Splitter.on(" ").trimResults().omitEmptyStrings();
    private RecordThread recordThread;

    public ScreenRecorder(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.SCREEN_VIDEO);
    }

    class RecordThread extends Thread {
        private Future<String> screenFuture = null;
        private Future<String> nextFuture = null;
        String captureCmd;
        private int count = 0;
        private static final long FIVE_SECONDS = 5000;
        private static final long THREE_MINUTES = 3 * 60 * 1000;

        RecordThread() {
            super("RecordScreen");
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                try {
                    captureCmd = String.format("%s %s/screen%d.mp4", SCREEN_CMD, mLoggerMain.getLogDir(), count);
                    LOG.info("Starting screen capture: number " + count);
                    nextFuture = mDevice.executeShellCommand(captureCmd, 30, TimeUnit.MINUTES);
                    if (screenFuture != null) {
                        Thread.sleep(FIVE_SECONDS);
                        screenFuture.cancel(true);
                    }
                    Thread.sleep((THREE_MINUTES));
                    screenFuture = nextFuture;
                } catch (Exception e) {
                    LOG.error(e.toString());
                    return;
                }
                count++;
            }
        }
    }

    @Override
    public boolean start() {
        recordThread = new RecordThread();
        recordThread.start();
        return true;
    }

    private void _stop() {
        try {
            LOG.info("Stopping screen capture");
            if (recordThread != null) {
                recordThread.interrupt();
                recordThread.join();
            }
            Future<String> psFuture = mDevice.executeShellCommand("ps | grep screenrecord", 10, TimeUnit.SECONDS);
            String res = psFuture.get();
            List<String> list = blankSplitter.splitToList(res);
            if (list != null && list.size() >= 2) {
                String pidStr = list.get(1);
                LOG.debug("pidStr=" + pidStr);
                Future<String> killFuture = mDevice.executeShellCommand("kill -2 " + pidStr, 10, TimeUnit.SECONDS);
                killFuture.get();   // Wait
            }
            LOG.warn("Failed to find pid from ps, will do process destroy");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        _stop();
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        _stop();
    }
}
