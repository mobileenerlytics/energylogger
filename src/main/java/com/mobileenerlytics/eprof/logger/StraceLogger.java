package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class StraceLogger extends EprofLogger {
    private static final Logger log = LoggerFactory.getLogger("StraceLogger");
    private static final String STRACE = "strace";
    private final String PS_GREP = "ps | grep strace";
    Future<String> straceFuture;

    public StraceLogger(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.STRACE);
    }

    @Override
    public void prepareDevice() throws Exception {
        mDevice.pushScript(getClass().getClassLoader().getResource(STRACE));
    }

    @Override
    public boolean start() {
        String packageName = this.mLoggerMain.getPackageName();
        String packagePID = PsLogger.getPackagePID(packageName);
//        if (packagePID == null) //todo: handle null
        try {
            straceFuture = mDevice.runScript(STRACE,
                    new String[]{"-o", mLoggerMain.getLogDir() + "/strace", "-tt", "-f", "-p", packagePID});
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    String getPIDs() {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> pids = PsLogger.getAllPIDs();
        for (String pid : pids) {
            stringBuilder.append(" -p ").append(pid);
        }
        return stringBuilder.toString();
    }

    public String getStracePID() throws Exception {
        Future<String> psGrep = mDevice.executeShellCommand(PS_GREP, 10, TimeUnit.SECONDS);
        String grepResult = psGrep.get();
        return grepResult.trim().split("\\s+")[1];
    }

    @Override
    public void stop() {
        _stop();
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        _stop();
    }

    private void _stop() {
        if(straceFuture != null)
            straceFuture.cancel(true);
        try {
            String stracePID = getStracePID();
            String resetCommand = "kill -INT " + stracePID;
            Future<String> future = mDevice.executeShellCommand(resetCommand, 10, TimeUnit.SECONDS);
            future.get();
        } catch (Exception e) {
            log.warn("Failed to kill -INT strace");
        }
    }
}
