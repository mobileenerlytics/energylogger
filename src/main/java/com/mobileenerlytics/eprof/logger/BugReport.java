package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Created by abhilash on 25/07/18.
 */

public class BugReport extends EprofLogger {
    private static final String CLEAN = "rm -rf ";
    private static final String MOVE = "mv ";
    private static final String[] PATHS = new String[]{"/data/user_de/0/com.android.shell/files/bugreports/*",
                                                       "/data/data/com.android.shell/files/bugreports/*"};
    private static final String CAPTURE_BUGREPORT = "setprop ctl.start bugreport";
    private static final String BUGREPORT_STATUS = "getprop init.svc.bugreport";
    public BugReport(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.HISTORIAN);
    }

    @Override
    public boolean start() {
        Future<String> future = null;
        try {
            for (String path : PATHS) {
                future = mDevice.executeShellCommand(CLEAN + path, 10, TimeUnit.SECONDS);
                future.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    Runnable stopRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                Future<String> future = mDevice.executeShellCommand(CAPTURE_BUGREPORT, 10, TimeUnit.SECONDS);
                future.get();
                future = mDevice.executeShellCommand(BUGREPORT_STATUS, 10, TimeUnit.SECONDS);

                while (future.get().startsWith("running")) {
                    Thread.sleep(5000);   // Poll status of bugreport every 5 second
                    future = mDevice.executeShellCommand(BUGREPORT_STATUS, 10, TimeUnit.SECONDS);
                }
                for (String path : PATHS) {
                    String move = String.format(MOVE + path + " %s/", mLoggerMain.getLogDir());
                    future = mDevice.executeShellCommand(move, 15, TimeUnit.SECONDS);
                    future.get();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mLoggerMain.finish(BugReport.this);
        }
    };

    @Override
    public void stop() {
        new Thread(stopRunnable).start();
    }

    @Override
    public void reset() {
        // Do nothing
    }
}
