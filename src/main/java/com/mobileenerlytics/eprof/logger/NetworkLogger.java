package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class NetworkLogger extends EprofLogger {
    private static final String TCPDUMP = "tcpdump";
    Future<String> tcpdumpFuture;
    private static final String TAG = TCPDUMP;
    private static final Logger logger = LoggerFactory.getLogger(TAG);
    Thread networkMonitorThread;

    class NetworkTableMonitorThread extends Thread {
        Map<String, String> conversationLines = new HashMap<>();
        private final int SLEEP_TIME = 5000;

        NetworkTableMonitorThread() {
            super("network-table-monitor");
        }

        @Override
        public void run() {
            while(!isInterrupted()) {
                try {
                    updateNetworkTable();
                    Thread.sleep(SLEEP_TIME);
                } catch (InterruptedException e) {
                    break;
                }
            }
            dumpConversations();
        }

        private void dumpConversations() {
            String tableFile =  mLoggerMain.getLogDir() + "/tcp";
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(tableFile))) {
                outputStreamWriter.write("This file is a combination of files /proc/net/tcp, tcp6, udp, udp6\n");
                for (String line : conversationLines.values()) {
                    outputStreamWriter.write(line);
                    outputStreamWriter.write("\n");
                }
            } catch (IOException e) {
                logger.error("Failed to write network table");
                e.printStackTrace();
            }
        }

        private void updateNetworkTable() throws InterruptedException {
            try {
                String[] fileNames = new String[]{"tcp", "tcp6", "udp", "udp6"};
                for (String fileName : fileNames) {
                    String command = "cat /proc/net/" + fileName;
                    String table = mDevice.executeShellCommand(command, 10, TimeUnit.SECONDS).get();
                    String[] lines = table.split("\\r?\\n");
                    for(String line : lines) {
                        String[] tokens = line.trim().split("\\s+");
                        if(tokens.length < 3 || tokens[0].equals("sl"))
                            // Ignore first line, ignore broken incomplete lines
                            continue;
                        conversationLines.put(tokens[1] + " " + tokens[2], line);
                    }
                }
                logger.info("Total %d conversations found", conversationLines.size());
            } catch (InterruptedException e) {
                throw e;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public NetworkLogger(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.NET);
    }

    @Override
    public void prepareDevice() throws Exception {
        mDevice.pushScript(getClass().getClassLoader().getResource(TCPDUMP));
    }

    @Override
    public boolean start() {
        try {
            tcpdumpFuture = mDevice.runScript(TCPDUMP,
                    new String[]{"-i", "any", "-p", "-s", "0", "-w", mLoggerMain.getLogDir() + "/capture.pcap"});
            networkMonitorThread = new NetworkTableMonitorThread();
            networkMonitorThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void stop() {
        _stop();
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        _stop();
    }

    private void _stop() {
        tcpdumpFuture.cancel(true);
        try {
            mDevice.executeShellCommand("pkill tcpdump", 10, TimeUnit.SECONDS).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(networkMonitorThread != null)
            networkMonitorThread.interrupt();
        try {
            if(networkMonitorThread != null)
                networkMonitorThread.join();
        } catch (InterruptedException e) {
            // Ignore
        }
        networkMonitorThread = null;
    }
}
