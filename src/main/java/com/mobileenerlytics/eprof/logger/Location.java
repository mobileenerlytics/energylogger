package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class Location extends EprofLogger {
    private static final String STOP = "dumpsys location > %s/location";

    public Location(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.GPS);
    }

    @Override
    public boolean start() {
        // Do nothing
        return true;
    }

    @Override
    public void stop() {
        try {
            String stop = String.format(STOP, mLoggerMain.getLogDir());
            Future<String> future = mDevice.executeShellCommand(stop, 3, TimeUnit.MINUTES);
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        // Do nothing
    }
}
