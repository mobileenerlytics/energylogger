package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain.LoggedComponent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

public class AppLogger extends EprofLogger {
	private static final String TAG = "AppLogger";
	private static final Logger logger = LoggerFactory.getLogger(TAG);
	
	private static final String startLoggingCmd = "am startservice " + EprofLoggerMain.appPkg + "/.LoggingService";
	private static final String stopLoggingCmd = "am broadcast -a  " + EprofLoggerMain.appPkg + ".STOP_LOGGING";
//	private static final String remoteDir = "/sdcard/Android/data/" + EprofLoggerMain.appPkg + "/files/";

	public AppLogger(EprofLoggerMain pStatus) {
		super(pStatus);
		forComponent = Sets.newHashSet(LoggedComponent.NET);
	}

	@Override
	public boolean start() {
//		try {
//			// start the SignalLogging
//			Future<String> future = mDevice.executeShellCommand(startLoggingCmd, 10, TimeUnit.SECONDS);
//			future.get();
//			//execCmd(startSignalLoggingCmd);
//			logger.info("start app logging OK");
//		} catch (Exception e) {
//			logger.error("error in start app logging");
//			logger.error(e.toString());
//		}
		return true;
	}
//
//	private void pullLog(String fileName) {
//		String localFile = mLoggerMain.getLogDir() + fileName;
//		try {
////			mDevice.pullFile(remoteDir + fileName, localFile);
//			logger.info(fileName + " pulled OK");
//		} catch (Exception e) {
//			logger.error(e.toString());
//		}
//	}
//
	@Override
	public void stop() {
//		_stop();
//		pullLog("signal_out");
//		pullLog("gps_out");
//		mLoggerMain.finish(this);
	}
//
//	@Override
	public void reset() {
//		_stop();
	}
//
//	private void _stop() {
//		try {
//			Future<String> future = mDevice.executeShellCommand(stopLoggingCmd, 10, TimeUnit.SECONDS);
//			future.get();
//			//execCmd(stopSignalLoggingCmd);
//			logger.info("stop app logging OK");
//		} catch (Exception e) {
//			logger.error("error in stop app logging");
//			logger.error(e.toString());
//		}
//	}
}
