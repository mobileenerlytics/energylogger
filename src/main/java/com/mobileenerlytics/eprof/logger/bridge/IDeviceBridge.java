package com.mobileenerlytics.eprof.logger.bridge;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public interface IDeviceBridge {
	String getName();

//	public void pushFile(String localPath, String remotePath) throws Exception;

//	public void pullFile(String logcatFile, String localLogcat) throws Exception;

	Future<String> executeShellCommand(String cmd, long maxTimeToOutputResponse,
			TimeUnit maxTimeUnits) throws Exception, IOException;

	//public CountDownLatch executeShellCommandToFile(String file, String cmd)
	//		throws Exception, IOException;

	//public CountDownLatch executeShellCommandToFile(String file, String cmd, long maxTimeToOutputResponse,
	//		TimeUnit maxTimeUnits) throws Exception, IOException;

	String getProperty(String property);

	ClientBridge[] getClients();

	void pushScript(URL url) throws Exception;

	Future<String> runScript(String scriptName, String[] args) throws IOException, Exception;

	int getNumCores();

	int getAPILevel();

	String getVersionRelease();

	String getCachePath();
}