package com.mobileenerlytics.eprof.logger.bridge;

public abstract class ClientBridge {
	public abstract String getPkgName();
	public abstract void kill();
}
