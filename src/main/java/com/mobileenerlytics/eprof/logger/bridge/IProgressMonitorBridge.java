package com.mobileenerlytics.eprof.logger.bridge;

public abstract class IProgressMonitorBridge {

	public abstract void beginTask(String task, int work);

	public abstract void subTask(String task);

	public abstract void worked(int work);

	public abstract void setCanceled(boolean isCancelled);

}
