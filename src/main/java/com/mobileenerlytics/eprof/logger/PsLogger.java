package com.mobileenerlytics.eprof.logger;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;

public class PsLogger extends EprofLogger {

	static ConcurrentMap<Integer, String> taskRecords;
	private static final String MYPS_T = "myps-t";
	private static final String TAG = "PsLogger";
	private static final Logger logger = LoggerFactory.getLogger(TAG);
	private static String zygoteID = null;
	Thread monitorThread;

	public PsLogger(EprofLoggerMain pStatus) {
		super(pStatus);
		forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.CPU);
	}

	@Override
	public void prepareDevice() throws Exception {
		URL mypsURL = getClass().getClassLoader().getResource(MYPS_T);
		mDevice.pushScript(mypsURL);
	};

	private boolean getPs(boolean dbg) {
		try {
			Future<String> psFuture = mDevice.runScript(MYPS_T, new String[]{"0"});
			String out = psFuture.get();
			String[] lines = out.split("\\r?\\n");
			for(String line : lines) {
				String[] tokens = line.split("\\s+");
				try {
					int task = Integer.parseInt(tokens[1]);
					taskRecords.put(task, line);
				} catch (NumberFormatException nfe) {
					// Ignore. First line of ps.
				} catch ( ArrayIndexOutOfBoundsException aibe) {
					// Ignore. Empty line. No tokens.
				}
			}
			if(dbg) {
				for(int task : taskRecords.keySet()) {
					logger.info(taskRecords.get(task) + "\r\n");
				}
			}
		} catch (Exception e1) {
			logger.error(e1.toString());
			return false;
		}
		return true;
	}

	private static String findZygotePID() {
		if (taskRecords == null || taskRecords.size() == 0) return null;
		for (String line : taskRecords.values()) {
			if (line.contains("zygote")) {
				return line.trim().split("\\s+")[1];
			}
		}
		return null;
	}

	static List<String> getAllPIDs() {
		if (taskRecords == null || taskRecords.size() == 0) return null;
		ArrayList<String> pids = new ArrayList<>();
		for (String line : taskRecords.values()) {
			pids.add(line.trim().split("\\s+")[1]);
		}
		return pids;
	}


	static String getPackagePID(String packageName) {
		if (taskRecords == null || taskRecords.size() == 0) return null;
		for (String line : taskRecords.values()) {
			if (line.contains(packageName)) {
				return line.trim().split("\\s+")[1];
			}
		}
		return null;
	}

	static public HashSet<String> findParentPIDs(String packageName) {
//		if (zygoteID == null || zygoteID.equals("")) {
//			zygoteID = findZygotePID();
//		}
//		logger.debug("ZygoteID=" + zygoteID);
		if (taskRecords == null || taskRecords.size() == 0) return null;
		if (packageName == null || packageName.equals("")) return null;
		HashSet<String> parentPIDs = new HashSet<>();
		for (String line : taskRecords.values()) {
			if (line.contains(packageName)) {
				String[] tokens =  line.trim().split("\\s+");
				String parentID = tokens[2];
				String packageID = tokens[1];
				logger.debug("LineMatch=" + line);
				logger.debug("ParentID=" + parentID);
				logger.debug("PackageID=" + packageID);
				parentPIDs.add(packageID);
//				if (parentID.equals(zygoteID)) {
//					parentPIDs.add(result);
//				}
			}
		}
		return parentPIDs;
	}

	static public HashSet<String> getValidPIDs(Set<String> parentPIDs) {
		if (taskRecords == null || taskRecords.size() == 0) return null;
		if (parentPIDs == null || parentPIDs.size() == 0) return null;
		HashSet<String> validPIDs = new HashSet<>();
		validPIDs.addAll(parentPIDs);
		for (String parentPID : parentPIDs) {
			for (String line: taskRecords.values()) {
				String[] procSplit = line.trim().split("\\s+");
				if (procSplit[2].equals(parentPID)) {
					validPIDs.add(procSplit[1]);
				}
			}
		}
		return validPIDs;
	}

	@Override
	public boolean start() {
		taskRecords = new ConcurrentHashMap<>();
		if(!getPs(false)){
			return false;
		}
		monitorThread = new Thread("ps-monitor thread") {
			int SLEEP_TIME = 5000;

			@Override
			public void run() {
				while (!isInterrupted()) {
					try {
						Thread.sleep(SLEEP_TIME);
						logger.debug("Monitor ps");
						if(!getPs(false))
							return;
					} catch (InterruptedException e) {
						return;
					}
				}
			};
		};
		logger.info("Starting ps-monitor thread");
		monitorThread.start();
		return true;
	}

	@Override
	public void stop() {
		_stop();
		try {
			FileWriter fw = new FileWriter(mLoggerMain.getLogDir() + "/ps_out", true);
			TreeMap<Integer, String> sortedTasks = new TreeMap<>(taskRecords);
			for(int task : sortedTasks.keySet()) {
				fw.write(sortedTasks.get(task) + "\r\n");
			}
			fw.close();
		} catch (IOException e) {
			logger.error(e.toString());
		}
		logger.info("Written {}", taskRecords.size());
		mLoggerMain.finish(this);
	}

	@Override
	public void reset() {
		_stop();
	}

	private void _stop() {
		monitorThread.interrupt();
		try {
			monitorThread.join();
		} catch (InterruptedException e1) {
			logger.error(e1.toString());
		}
		logger.info("Stopped ps-monitor thread.");
	}
}
