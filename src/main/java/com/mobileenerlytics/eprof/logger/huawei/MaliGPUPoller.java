package com.mobileenerlytics.eprof.logger.huawei;

import com.google.common.collect.Sets;
import com.mobileenerlytics.eprof.logger.Atrace;
import com.mobileenerlytics.eprof.logger.EprofLogger;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class MaliGPUPoller extends EprofLogger {
    private static final String GPU_BASEDIR = "/sys/devices/platform/e8970000.mali/devfreq/gpufreq/";
    private static final String GET_STAT = "cat " + GPU_BASEDIR + "trans_stat";
    private Map<Long, Long> freqTimeMap = new HashMap<>();
    PollThread thread;

    public MaliGPUPoller(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.GPU);
    }

    class PollThread extends Thread {
        @Override
        public void run() {
            while(!isInterrupted()) {
                try {
                    Future<String> future = mDevice.executeShellCommand(GET_STAT,
                            10, TimeUnit.SECONDS);
                    String stats = future.get();
                    String[] lines = stats.split("\\n");
                    StringBuilder log = new StringBuilder();
                    for(String line: lines) {
                        line = line.trim();
                        String[] tokens = line.split("\\s+");
                        try {
                            long timeMs = Long.parseLong(tokens[tokens.length - 1]);
                            // Delete non-digit characters like * and :
                            String freqStr = tokens[0].replaceAll("\\D+","");
                            long freq = Long.parseLong(freqStr);
                            if(freqTimeMap.containsKey(freq)) {
                                long duration = timeMs - freqTimeMap.get(freq);
                                if (duration > 0)
                                    log.append(String.format("%d-%d,", freq, duration));
                            }
                            freqTimeMap.put(freq, timeMs);
                        } catch(NumberFormatException e) {
                            // Ignore
                        }
                    }
                    if(log.length() != 0) {
                        File logFile = new File(Atrace.tracingDir + "trace_marker");
                        try( FileWriter writer = new FileWriter(logFile)) {
                            writer.write("gpu ");
                            writer.write(log.toString());
                        }
                    }
                    Thread.sleep(500);
                } catch (Exception e) {
                    if(e instanceof InterruptedException)
                        break;
                    // Otherwise ignore
                    e.printStackTrace();

                }
            }
        }
    }

    @Override
    public boolean start() {
        thread = new PollThread();
        thread.start();
        return true;
    }

    @Override
    public void stop() {
        stop_();
        mLoggerMain.finish(this);;
    }

    void stop_() {
        try {
            if(thread != null) {
                thread.interrupt();
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reset() {
        stop_();
    }


}
