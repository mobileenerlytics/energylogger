package com.mobileenerlytics.eprof.logger.huawei;

import com.google.common.collect.Sets;
import com.mobileenerlytics.eprof.logger.EprofLogger;
import com.mobileenerlytics.eprof.logger.EprofLoggerMain;
import com.mobileenerlytics.eprof.logger.util.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class HwScreenRecorder extends EprofLogger {

    private static final String START = "am startservice -a com.huawei.screenrecorder.Start";
    private static final String STOP = "am startservice -a com.huawei.screenrecorder.Stop";
    private static final Logger logger = LoggerFactory.getLogger("HwScreenRecorder");

    public HwScreenRecorder(EprofLoggerMain loggerMain) {
        super(loggerMain);
        forComponent = Sets.newHashSet(EprofLoggerMain.LoggedComponent.SCREEN_VIDEO);
    }

    @Override
    public boolean start() {
        try {
            Future<String> future = mDevice.executeShellCommand(START, 10, TimeUnit.SECONDS);
            future.get();
            // Wait for 3.. 2 .. 1
            Thread.sleep(4000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private void _stop() {
       try {
            Future<String> future = mDevice.executeShellCommand(STOP, 10, TimeUnit.SECONDS);
            future.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        _stop();
        // Move the file to log folder
        try {
            Future<String> future = mDevice.executeShellCommand("echo $EXTERNAL_STORAGE", 10, TimeUnit.SECONDS);
            String sdcard = future.get().trim();
            File src = Util.lastFileModified(sdcard + "/Pictures/Screenshots/");
            File dst = new File(mLoggerMain.getLogDir() + "screen0.mp4");
            src.renameTo(dst);
        } catch (Exception e) {
            logger.warn("Something went wrong in moving the screen recording");
            e.printStackTrace();
        }
        mLoggerMain.finish(this);
    }

    @Override
    public void reset() {
        _stop();
    }
}
