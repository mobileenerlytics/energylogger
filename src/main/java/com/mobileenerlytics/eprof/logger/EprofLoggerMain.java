package com.mobileenerlytics.eprof.logger;

import com.google.common.base.Preconditions;
import com.mobileenerlytics.eprof.logger.bridge.ClientBridge;
import com.mobileenerlytics.eprof.logger.bridge.IDeviceBridge;
import com.mobileenerlytics.eprof.logger.bridge.IProgressMonitorBridge;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

public class EprofLoggerMain {
    private static final String VERSION_RELEASE = "versionRelease";
    private static final String API_LEVEL = "apiLevel";
    private static final String CPU_CORES = "cpu.cores";
    public static final String PROP_DEVICE = "ro.product.device";
    public final static String appPkg = "com.mobileenerlytics.eproflogger";
    public final static String TIME_ZONE = "timezone";
    public final static String PACKAGE_NAME = "packageName";
    private static final String DURATION_MS = "durationInMs";
    private static final String START_TIME = "startTime";
    private static final String END_TIME = "endTime";
    private String packageName = "";
    private long startTime;
    private long endTime;
    private Map<String, String> buildProperties;

    public enum LoggedComponent {

        CPU (0x1), GPU (0x2), NET (0x4), GPS (0x8), POWER_EVENT (0x10), SCREEN_VIDEO (0x20),
        LOGCAT (0x40), HISTORIAN (0x80), STRACE (0x100), CURRENT_SENSOR (0x200), ALL (0x0);
        public int bitmap;

        LoggedComponent(int bitmap) {
            this.bitmap = bitmap;
        }

        static int getMap(LoggedComponent... logs) {
            int ret = 0;
            for (LoggedComponent log: logs) {
                ret |= log.bitmap;
            }
            return ret;
        }
    }

    static final String GET_TIME = "get_time";
    private static final String TAG = "EprofLoggerMain";
    private static final Logger log = LoggerFactory.getLogger(TAG);
    private final int STOP_WORK = 1;
    List<IEprofChangeListener> mEprofListeners;
    public AsyncLogger asyncLogger;
    List<EprofLogger> eprofLoggers;
    Set<EprofLogger> pendingLoggers;
    IDeviceBridge mDevice;
    String logDir = null;
    String traceMarkerPath = "/d/tracing/trace_marker";
    String frequencyDir = "/sys/devices/system/cpu/";

    enum EprofState {
        STARTED, STOPPED;
    }

    EprofState state;

    private Set<LoggedComponent> mEnabledComponents;

    public boolean isComponentEnabled(Collection<LoggedComponent> comp) {
        return !Collections.disjoint(mEnabledComponents, comp);
    }

    public EprofLoggerMain(IDeviceBridge device) {
        state = EprofState.STOPPED;
        log.info("init. state: {}", state);
        eprofLoggers = new LinkedList<EprofLogger>();
        asyncLogger = new AsyncLogger(this);
        mDevice = device;
    }

    public void init(List<EprofLogger> eprofLoggers_, boolean isExperimental) {
        eprofLoggers.add(new LogcatManager(this));
        eprofLoggers.add(new BatteryStats(this));
        eprofLoggers.add(new Location(this));
        eprofLoggers.add(new Atrace(this));
        eprofLoggers.add(new PsLogger(this));
        eprofLoggers.addAll(EprofLogger.getDeviceSpecificLogger(this, mDevice.getProperty(PROP_DEVICE)));
        eprofLoggers.add(new StraceLogger(this));
        eprofLoggers.add(new NetworkLogger(this));
        eprofLoggers.add(new BugReport(this));
        eprofLoggers.addAll(eprofLoggers_);

        if(isExperimental) {
            // eprofLoggers.add(new AppLogger(this));
        }
    }

    public synchronized void prepareDevice() throws Exception {
        URL getTimeURL = getClass().getClassLoader().getResource(GET_TIME);
        mDevice.pushScript(getTimeURL);
        for (EprofLogger logger : eprofLoggers) {
            logger.prepareDevice();
        }
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public long getStartTimeMillis() {
        return this.startTime;
    }

   public synchronized boolean start(String logDir_, boolean killApps, Set<String> whiteList,
                                      Set<LoggedComponent> enabledComponents, @Nullable Map<String, String> buildProps,
                                      @Nullable IProgressMonitorBridge progMonitor) {
        Preconditions.checkArgument(enabledComponents != null && enabledComponents.contains(LoggedComponent.CPU));
        if (state != EprofState.STOPPED) {
            log.warn("Attempted to start already started eprof " + state);
            return false;
        }

        mEnabledComponents = enabledComponents;
        logDir = logDir_;
        pendingLoggers = new HashSet<>();
        int numLoggers = eprofLoggers.size();

        // + 1 for initial stuff
        if (progMonitor != null)
            progMonitor.beginTask("Starting eprof tracing...", numLoggers + 1);
        {
            buildProperties = buildProps;
            if (progMonitor != null)
                progMonitor.subTask("Initialize");
            if (killApps) {
                if (progMonitor != null)
                    progMonitor.subTask("Killing apps");
                killAllApps(whiteList);
            }

            if (progMonitor != null)
                progMonitor.worked(1);
        }


        boolean success = true;

        if (progMonitor != null)
            progMonitor.subTask("Starting loggers");
        for (EprofLogger logger : eprofLoggers) {
            if (isComponentEnabled(logger.forComponent)) {
                pendingLoggers.add(logger);
                success = logger.start();
                if (!success) {
                    break;
                }
            } else {
                log.debug("Skipping ", logger.getClass());
            }
            if (progMonitor != null)
                progMonitor.worked(1);
        }
        if (success) {
            state = EprofState.STARTED;
            log.info("Started. state: {}", state);
            startTime = System.currentTimeMillis();
            log.info("Started time (ms): {}", startTime);
            this.writeCurrentFrequencies();
            notifyStart();
            return true;
        } else {
            for (EprofLogger logger : pendingLoggers) {
                if (isComponentEnabled(logger.forComponent)) {
                    logger.reset();
                }
            }
            pendingLoggers = null;
            if (progMonitor != null)
                progMonitor.setCanceled(true);
            state = EprofState.STOPPED;
            log.info("Failed to start. state: {}", state);
            notifyStop(false);
            return false;
        }
    }

    public void writeCurrentFrequencies() {
        for (int i = 0; i < mDevice.getNumCores(); i++) {
            String path = frequencyDir + "cpu" + i + "/cpufreq/scaling_cur_freq";
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))){
                String cpuLine = bufferedReader.readLine();
                String writeCommand = "echo 'cpu_frequency: state=" + cpuLine + " cpu_id=" + i + "' > " + traceMarkerPath;
                Future<String>  writeTraceMarker = mDevice.executeShellCommand(writeCommand, 10, TimeUnit.SECONDS);
                writeTraceMarker.get();
            } catch (Exception e) {
                log.error(e.toString());
            }
        }
    }

    private void writeBuildProp(@Nullable Map<String, String> buildProperties) {
        try {
            PrintWriter buildPropWriter = new PrintWriter(getLogDir() + "build.prop", "UTF-8");
            buildPropWriter.println(mDevice.getName());

            int cpus = mDevice.getNumCores();
            buildPropWriter.println(VERSION_RELEASE + "=" + mDevice.getVersionRelease());
            buildPropWriter.println(API_LEVEL + "=" + mDevice.getAPILevel());
            buildPropWriter.println(CPU_CORES + "=" + cpus);
            buildPropWriter.println(PROP_DEVICE + "=" + mDevice.getProperty(PROP_DEVICE));
            buildPropWriter.println(TIME_ZONE + "=" + TimeZone.getDefault().getID());
            buildPropWriter.println(PACKAGE_NAME + "=" + packageName);
            buildPropWriter.println(START_TIME + "=" + startTime);
            buildPropWriter.println(END_TIME + "=" + endTime);
            long duration = (endTime - startTime);
            buildPropWriter.println(DURATION_MS + "=" + duration);

            if (buildProperties != null) {
                for (Entry<String, String> entry : buildProperties.entrySet()) {
                    buildPropWriter.println(entry.getKey() + "=" + entry.getValue());
                }
            }
            buildPropWriter.close();
        } catch (Exception e) {
            log.error(e.toString());
        }
    }

    private void killAllApps(Set<String> whiteList) {
        // First kill ALL the clients to control overhead
        ClientBridge[] clients = mDevice.getClients();
        int killed = 0;
        for (ClientBridge client : clients) {
            if (!whiteList.contains(client.getPkgName())) {
                client.kill();
            }
            killed++;
        }
        try {
            Thread.sleep(2000); // Wait for all clients to exit
        } catch (InterruptedException e) {
        }
    }

    @Nullable
    IProgressMonitorBridge stopProgMonitor;

    synchronized public void stop(@Nullable IProgressMonitorBridge progMonitor) {
        if (state != EprofState.STARTED) {
            log.warn("Attempted to stop already stopped eprof " + state);
            return;
        }
        stopProgMonitor = progMonitor;
        if (progMonitor != null) {
            progMonitor.beginTask("Stopping Eagle Debugger", STOP_WORK + pendingLoggers.size());
            progMonitor.subTask("Initialize");
        }
        // Stopping eprof
        endTime = System.currentTimeMillis();
        log.info("Stopping time (ms): {}", endTime);
        writeBuildProp(buildProperties);
        try {
            if (progMonitor != null)
                progMonitor.subTask("Stopping loggers");

            // Stop in reverse direction. So we get the full logcat output
            ListIterator<EprofLogger> li = eprofLoggers.listIterator(eprofLoggers.size());
            while (li.hasPrevious()) {
                EprofLogger logger = li.previous();
                if (isComponentEnabled(logger.forComponent)) {
                    logger.stop();
                } else {
                    if (pendingLoggers.contains(logger)) {
                        throw new AssertionError(
                                "Pending logger contains disabled logger while stopping" + logger.getClass());
                    }
                }
            }
            if (progMonitor != null)
                progMonitor.worked(STOP_WORK);
        } catch (Exception e) {
            if (progMonitor != null)
                progMonitor.setCanceled(true);
            state = EprofState.STOPPED;
            log.info("Exception in stopping. state {}", state);
            e.printStackTrace();
            notifyStop(false);
        }
    }

    /**
     * This method should not be called from a newly created thread.
     * This method will eventually call stoppedEprof that creates a Toast.
     * Toast fails to create if called from a random thread.
     *
     * @param logger
     */
    public synchronized void finish(EprofLogger logger) {
        if (EprofState.STARTED == state) {
            if (pendingLoggers.isEmpty()) {
                throw new AssertionError("Pending loggers are already empty at finish " + logger);
            }

            pendingLoggers.remove(logger);
            if (stopProgMonitor != null)
                stopProgMonitor.worked(1);

            log.info("finish: {} pending loggers {}", logger.getClass().getSimpleName(), pendingLoggers.size());
            if (pendingLoggers.isEmpty()) {
                pendingLoggers = null;
                state = EprofState.STOPPED;
                log.info("Finished. state {}", state);
                notifyStop(true);
            }
        } else {
            log.warn("Something may be wrong. Finish called even when state is {}", state);
        }
    }

    synchronized public void reset(@Nullable IProgressMonitorBridge progMonitor) {
        if (progMonitor != null)
            progMonitor.subTask("Initialize");
        // Stopping eprof
        try {
            if (progMonitor != null)
                progMonitor.subTask("Resetting loggers");
            for (EprofLogger logger : eprofLoggers) {
                logger.reset();
            }
            if (progMonitor != null)
                progMonitor.worked(STOP_WORK);
        } catch (Exception e) {
            if (progMonitor != null)
                progMonitor.setCanceled(true);
            throw e;
        } finally {
            state = EprofState.STOPPED;
            log.info("reset. state {}", state);
            notifyReset();
        }
    }

    void notifyStart() {
        if (mEprofListeners != null) {
            for (IEprofChangeListener eprofListener : mEprofListeners) {
                eprofListener.startedEprof();
            }
        }
    }

    void notifyStop(boolean success) {
        startTime = 0;
        if (mEprofListeners != null) {
            for (IEprofChangeListener eprofListener : mEprofListeners) {
                eprofListener.stoppedEprof(success, logDir);
            }
        }
    }

    public void notifyProcessed(boolean success) {
        if (mEprofListeners != null) {
            for (IEprofChangeListener eprofListener : mEprofListeners) {
                eprofListener.processedTraces(success);
            }
        }
    }

    void notifyReset() {
        if (mEprofListeners != null) {
            for (IEprofChangeListener eprofListener : mEprofListeners) {
                eprofListener.resettedEprof(logDir);
            }
        }
    }

    public void addEprofChangeListener(IEprofChangeListener listener) {
        if (mEprofListeners == null) {
            mEprofListeners = new LinkedList<IEprofChangeListener>();
        }
        mEprofListeners.add(listener);
    }

    public void removeEprofChangeListener(IEprofChangeListener listener) {
        if (mEprofListeners != null) {
            mEprofListeners.remove(listener);
        }
    }

    public void addApp(ClientBridge client) {
        if (state == EprofState.STARTED) {
            for (EprofLogger logger : eprofLoggers) {
                logger.addClient(client);
            }
        }
    }

    public void removeApp(ClientBridge client) {
        if (state == EprofState.STARTED) {
            for (EprofLogger logger : eprofLoggers) {
                logger.removeClient(client);
            }
        }
    }

    public String getLogDir() {
        return logDir;
    }

    public String getDeviceTime() {
        // TODO
//        try {
//            Future<String> future = mDevice.executeShellCommand(REMOTE_TMP + GET_TIME, 10, TimeUnit.SECONDS);
//            return future.get();
//        } catch (Exception e) {
//            log.error(e.toString());
//            return null;
//        }
        return null;
    }

    public boolean isEprofRunning() {
        return (state == EprofState.STARTED);
    }

}
